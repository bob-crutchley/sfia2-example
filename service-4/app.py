from flask import Flask
import random
import string
import requests
app = Flask(__name__)

@app.route('/')
def index():
    response = requests.get('http://service-2:5000')
    random_number = response.text
    response = requests.get('http://service-3:5000')
    random_letter = response.text
    return str(random_letter + random_number)

if __name__ == '__main__':
    app.run(debug=True, host='0.0.0.0')

