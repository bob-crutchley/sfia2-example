# Random Coordinates
## Service 1
Front end client, makes a request to Service 4 for coordinates.
## Service 2
- Generate a random number.
## Service 3
- Generate a random letter.
## Service 4
- Get a random number from Service 2.
- Get a random letter from Service 3.
- Return a combination of the responses from Services 2 & 3.

