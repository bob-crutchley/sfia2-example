from flask import Flask
import random
import string
app = Flask(__name__)

@app.route('/')
def index():
    random_letter = random.choice(string.ascii_uppercase)
    return str(random_letter)

if __name__ == '__main__':
    app.run(debug=True, host='0.0.0.0')

